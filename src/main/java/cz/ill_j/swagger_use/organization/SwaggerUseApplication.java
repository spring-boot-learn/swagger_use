package cz.ill_j.swagger_use.organization;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class SwaggerUseApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwaggerUseApplication.class, args);
    }

}
