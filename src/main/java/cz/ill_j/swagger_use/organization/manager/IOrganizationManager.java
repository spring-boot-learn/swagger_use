package cz.ill_j.swagger_use.organization.manager;

import java.util.List;

import cz.ill_j.swagger_use.organization.web.dto.OrganizationDto;

public interface IOrganizationManager {

    List<OrganizationDto> getAllOrganizations();
}
