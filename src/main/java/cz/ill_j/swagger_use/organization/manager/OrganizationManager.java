package cz.ill_j.swagger_use.organization.manager;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cz.ill_j.swagger_use.organization.configuration.OrganizationProperties;
import cz.ill_j.swagger_use.organization.model.Organization;
import cz.ill_j.swagger_use.organization.web.dto.OrganizationDto;

@Component
public class OrganizationManager implements IOrganizationManager {

    private OrganizationProperties properties;
    
    @Autowired
    public OrganizationManager(OrganizationProperties properties) {
        this.properties = properties;
    }

    @Override
    public List<OrganizationDto> getAllOrganizations() {
        List<Organization> organizations = properties.getOrganizations();
        // dto
        List<OrganizationDto> dtos = new ArrayList<>();
        for (Organization organization : organizations) {
            OrganizationDto dto = new OrganizationDto();
            dto.setDtoName(organization.getName());
            dto.setDtoAddress(organization.getAddress());
            // add to List
            dtos.add(dto);
        }
        return dtos;
    }

}
