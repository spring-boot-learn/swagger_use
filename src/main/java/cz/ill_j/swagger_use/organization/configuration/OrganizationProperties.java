package cz.ill_j.swagger_use.organization.configuration;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import cz.ill_j.swagger_use.organization.model.Organization;

@Component
@ConfigurationProperties("organizations")
public class OrganizationProperties {

    List<Organization> organizations;

    public List<Organization> getOrganizations() {
        return organizations;
    }

    public void setOrganizations(List<Organization> organizations) {
        this.organizations = organizations;
    }
}
