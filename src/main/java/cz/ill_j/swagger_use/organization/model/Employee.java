package cz.ill_j.swagger_use.organization.model;

/**
 * Employee bean
 * @author ill_j
 *
 */
public class Employee {

    private String firstNme;

    private String lastName;

    private JobPosition jobPosition;

    public String getFirstNme() {
        return firstNme;
    }
    public void setFirstNme(String firstNme) {
        this.firstNme = firstNme;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public JobPosition getJobPosition() {
        return jobPosition;
    }
    public void setJobPosition(JobPosition jobPosition) {
        this.jobPosition = jobPosition;
    }
    
}
