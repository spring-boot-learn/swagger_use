package cz.ill_j.swagger_use.organization.model;

public enum JobPosition {
    DIRECTOR,
    MANAGER,
    OPERATION
}
