package cz.ill_j.swagger_use.organization.model;

import java.util.List;

/**
 * Department bean
 * @author ill_j
 *
 */
public class Department {

    private String departmentName;

    private List<Employee> employeeList;

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }
    
}
