package cz.ill_j.swagger_use.organization.web.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cz.ill_j.swagger_use.organization.manager.IOrganizationManager;
import cz.ill_j.swagger_use.organization.web.dto.OrganizationDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

@Api(tags = { "Organization" }, value = "API for list organization details.")
@RestController
@RequestMapping(path = "/api/swagger_use" )
public class OrganizationController {

    private static final Logger log = LoggerFactory.getLogger(OrganizationController.class);

    private static final String ORGANIZATIONS_PATH = "/organizations";
    
    private IOrganizationManager organizationManager;
    
    @Autowired
    public OrganizationController(IOrganizationManager organizationManager) {
        this.organizationManager = organizationManager;
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Return all saved organizations", responseHeaders = @ResponseHeader(name = "Content-length", description = "Return list of organizations.", response = ResponseEntity.class)),
            @ApiResponse(code = 500, message = "Internal server error") })
    @GetMapping(value = ORGANIZATIONS_PATH, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Return list of organizations.", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<OrganizationDto>> getOrganizations() {
        List<OrganizationDto> dtoList = null;
        HttpStatus httpStatus;

        try {
            dtoList = organizationManager.getAllOrganizations();
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            log.error("Can't get list of organizations", e);
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return new ResponseEntity<List<OrganizationDto>>(dtoList, httpStatus);
    }
}
