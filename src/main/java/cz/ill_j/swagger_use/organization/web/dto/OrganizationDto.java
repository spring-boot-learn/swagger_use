package cz.ill_j.swagger_use.organization.web.dto;

public class OrganizationDto {

    private String dtoName;

    private String dtoAddress;

    public String getDtoName() {
        return dtoName;
    }

    public void setDtoName(String dtoName) {
        this.dtoName = dtoName;
    }

    public String getDtoAddress() {
        return dtoAddress;
    }

    public void setDtoAddress(String dtoAddress) {
        this.dtoAddress = dtoAddress;
    }
}
