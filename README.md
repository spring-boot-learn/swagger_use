# Swagger in use

## Install
- change server.port in application.yml
- mvn spring-boot:run

## Swagger documentation

```
    http://[host]:[port]/v2/api-docs
```
You can use https://editor.swagger.io for example.

```
    Under construction!
```